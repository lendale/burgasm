package com.burgasm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.EditText;


public class BurgasmSplash extends Activity {

    private static final int SECONDS = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_burgasm_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(BurgasmSplash.this, BurgasmLogin.class);
                BurgasmSplash.this.startActivity(intent);BurgasmSplash.this.startActivity(intent);
                BurgasmSplash.this.finish();
            }
        }, SECONDS);
    }
}
