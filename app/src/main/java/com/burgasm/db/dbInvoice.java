package com.burgasm.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.burgasm.model.Invoice;
import com.burgasm.model.Products;

/**
 * Created by Christian on 10/11/2015.
 */
public class dbInvoice extends SQLiteOpenHelper {

    // ---------------------------------------------------------------------------------------------
    // Database Settings
    // ---------------------------------------------------------------------------------------------
    private static final int    DB_VERSION = 1;
    private static final String DB_NAME    = "invoice.db";

    // ---------------------------------------------------------------------------------------------
    // Contacts Schema
    // ---------------------------------------------------------------------------------------------
    private static final String TABLE_INVOICE = "invoice";
    private static final String KEY_ID         = "invoice_id";
    private static final String KEY_TOTAL_PRICE = "invoice_total_price";
    private static final String KEY_USERNUMBER = "usernumber";

    private static final String[] PROJECTIONS_INVOICE = {KEY_ID, KEY_TOTAL_PRICE, KEY_USERNUMBER};

    private static final int KEY_ID_INDEX       = 0;
    private static final int KEY_TOTAL_PRICE_INDEX = 1;
    private static final int KEY_USERNUMBER_INDEX = 2;

    // ---------------------------------------------------------------------------------------------
    // SQL STATEMENTS
    // ---------------------------------------------------------------------------------------------
    private static final String CREATE_TABLE_INVOICE = "CREATE TABLE IF NOT EXISTS invoice (" +
            KEY_ID + " INTEGER PRIMARY KEY," +
            KEY_TOTAL_PRICE + " NUMBER," +
            KEY_USERNUMBER + " NUMBER)";

    Context context;
    public dbInvoice(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        SQLiteDatabase sb = getReadableDatabase();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_INVOICE);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTable(TABLE_INVOICE);
        onCreate(db);
    }

    // ---------------------------------------------------------------------------------------------
    // CRUD (Create, Read, Update, Delete) Operations
    // ---------------------------------------------------------------------------------------------
    public void addInvoice(Invoice invoice) {
        if (invoice == null) {
            return;
        }

        SQLiteDatabase db = getWritableDatabase();

        if (db == null) {
            return;
        }

        ContentValues cv = new ContentValues();
        cv.put(KEY_TOTAL_PRICE, invoice.getTotalprice());
        cv.put(KEY_USERNUMBER, invoice.getUserNumber());
        // Inserting Row
        db.insert(TABLE_INVOICE, null, cv);
        db.close();
    }

    public Invoice getInvoice(int id) {
        SQLiteDatabase db = getReadableDatabase();

        if (db == null) {
            return null;
        }

        Cursor cursor = db.query(TABLE_INVOICE, PROJECTIONS_INVOICE, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (!cursor.moveToFirst()) {
            return null;
        }

       Invoice invoice = new Invoice(cursor.getInt(KEY_ID_INDEX),
                cursor.getString(KEY_USERNUMBER_INDEX), cursor.getDouble(KEY_TOTAL_PRICE_INDEX));

        cursor.close();

        return invoice;
    }

    public List<Invoice> getAllInvoice() {
        List<Invoice> invoices = new ArrayList<>();

        // Selection
        String selectQuery = "SELECT * FROM " + TABLE_INVOICE;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(KEY_ID_INDEX);
                double price = cursor.getDouble(KEY_TOTAL_PRICE_INDEX);
                String number = cursor.getString(KEY_USERNUMBER_INDEX);
                Invoice invoice= new Invoice(id,number,price);
                invoices.add(invoice);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return invoices;
    }

    public int updateInvoice(Invoice invoice) {
        if (invoice == null) {
            return -1;
        }

        SQLiteDatabase db = getWritableDatabase();

        if (db == null) {
            return -1;
        }

        ContentValues cv = new ContentValues();
        cv.put(KEY_TOTAL_PRICE, invoice.getTotalprice());
        cv.put(KEY_USERNUMBER, invoice.getUserNumber());


        // Upating the row
        int rowCount = db.update(TABLE_INVOICE, cv, KEY_ID + "=?",
                new String[]{String.valueOf(invoice.getInvoiceId())});

        db.close();

        return rowCount;
    }

    public void deleteInvoice(Invoice invoice) {
        if (invoice == null) {
            return;
        }

        SQLiteDatabase db = getWritableDatabase();

        if (db == null) {
            return;
        }

        db.delete(TABLE_INVOICE, KEY_ID + "=?", new String[]{String.valueOf(invoice.getInvoiceId())});
        db.close();
    }

    public int getInvoiceCount() {
        String query = "SELECT * FROM  " + TABLE_INVOICE + ";";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();

        cursor.close();

        return count;
    }

    public void dropTable(String tableName) {
        SQLiteDatabase db = getWritableDatabase();

        if (db == null || TextUtils.isEmpty(tableName)) {
            return;
        }
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVOICE);
    }
}

