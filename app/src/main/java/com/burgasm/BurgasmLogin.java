package com.burgasm;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;


public class BurgasmLogin extends Activity implements TextToSpeech.OnInitListener  {

    private EditText mEtInput;
    private EditText mEtNumber;
    private TextToSpeech mTts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_burgasm_login);

        mEtInput = (EditText) findViewById(R.id.full_name);
        mEtNumber = (EditText) findViewById(R.id.phone_umber);

         mTts = new TextToSpeech(this, this);

    }


    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = mTts.setLanguage(Locale.ENGLISH);

            if (result == TextToSpeech.LANG_MISSING_DATA ||
                    result == TextToSpeech.LANG_NOT_SUPPORTED) {
//                Toast.makeText(this, R.string.tts_lang_not_supported, Toast.LENGTH_LONG).show();

                mEtInput.setEnabled(false);
            } else {
//                Toast.makeText(this, R.string.ready_to_speak, Toast.LENGTH_LONG).show();


                mEtInput.setEnabled(true);
            }
        } else {
//            Toast.makeText(this,R.string.cannot_speak, Toast.LENGTH_LONG).show();


            mEtInput.setEnabled(false);
        }
    }



    private void speak(String text) {
        if (TextUtils.isEmpty(text)) {
            mEtInput.setError(getString(R.string.error_text_empty));
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            mTts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    protected void onDestroy() {
        if (mTts != null) {
            mTts.stop();
            mTts.shutdown();
        }
        super.onDestroy();
    }

    public void btnProceed(View v){

        if(mEtInput.getText().length() == 0){
            mEtInput.setError("Please enter name.");
        }
        else if(mEtNumber.getText().length() != 11){
            mEtNumber.setError("Please enter number.");
        }
        else {
            String say = "Hi" + mEtInput.getText().toString() + "Welcome to Burgasm! May I have your order?";

            speak(say);

            Intent intent = new Intent(this, CategoryActivity.class);
            intent.putExtra("CELL_NUMBER", mEtNumber.getText());
            startActivity(intent);
        }

    }

    public void btnHowTo(View view){
        Intent intent = new Intent(this, Instructions.class);
        startActivity(intent);
    }

}
